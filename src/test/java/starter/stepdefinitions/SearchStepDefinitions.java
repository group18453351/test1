package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import org.hamcrest.Matchers;

import ch.qos.logback.classic.Logger;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.contains;

public class SearchStepDefinitions {

    @Steps
    public CarsAPI carsAPI;

    @When("he calls endpoint for orange {string}")
    public void heCallsEndpoint(String arg0) {
        SerenityRest.given().get(arg0);
    }

    @Then("he sees the results displayed for orange")
    public void heSeesTheResultsDisplayedForApple() {
        restAssuredThat(response -> response.statusCode(200));
    }
    
    @When("he calls endpoint for apple {string}")
    public void heCallsEndpoint1(String arg0) {
        SerenityRest.given().get(arg0);
    }


    @Then("he sees the results displayed for apple")
    public void heSeesTheResultsDisplayedForMango() {
    	restAssuredThat(response -> response.statusCode(200));

    }

    @When("he calls endpoint car {string}")
    public void heCallsEndpoint2(String arg0) {
        SerenityRest.given().get(arg0);
    }

    @Then("he doesn not see the results")
    public void he_Doesn_Not_See_The_Results() {
    	restAssuredThat(response -> response.toString());
    	System.out.println("Negative scenario covered");
       
    }
    @When("he calls endpoint for car {string}")
    public void he_calls_endpoint_for_car(String string) throws Exception {
        // Write code here that turns the phrase above into concrete actions
    	restAssuredThat(response -> response.statusCode(200));
       
    }
}
